package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"sync"

    "github.com/vjeantet/jodaTime"
    "gopkg.in/cheggaaa/pb.v1"
)

var duration int
var allRes string
var bar *pb.ProgressBar
var lastPer int
var movesToConvert []string

func init() {
	lastPer = -1
	movesToConvert = make([]string, 0)
}

/*
* Converts hours/minutes to seconds
*/
func toSeconds(dur string) (int, error) {
	t, err := jodaTime.Parse("HH:mm:ss", dur)

	if err != nil {
		return 0, err
	}

	sec := t.Hour() * (60 * 60)
	sec += t.Minute() * (60)
	sec += t.Second()
	return sec, nil
}

/*
* Search for Duration/time tags in string
*/
func parseString(res string) {

	if duration == 0 {
		DurationIndex := strings.Index(res, "Duration")
		if DurationIndex >= 0 {

			dur := res[DurationIndex+10:]
			if len(dur) > 8 {
				dur = dur[0:8]

				var err error
				duration, err = toSeconds(dur)
				if err != nil {
					fmt.Println(fmt.Sprintf("Error in toSeconds: %s", err.Error()))
				}
				fmt.Println("duration:", duration)

				// start new progressbar
				bar = pb.New(100)
				bar.ShowPercent = true
				bar.ShowBar = true
				bar.Start()

				allRes = ""
			}
		}

	} else {

		timeIndex := strings.Index(res, "time=")
		if timeIndex >= 0 {

			timeString := res[timeIndex+5:]
			if len(timeString) > 8 {
				timeString = timeString[0:8]
				sec, err := toSeconds(timeString)

				if err != nil {
					fmt.Println(fmt.Sprintf("Error in toSeconds: %s", err.Error()))
				}

				per := (sec * 100) / duration
				if lastPer != per {
					lastPer = per

					if lastPer == 100 {
						bar.Finish()
					} else {
						bar.Increment()
					}
				}

				allRes = ""
			}
		}
	}
}

/*
* One of the channels to get more of files to convert
* can be replaced to get data from database or queue
*/
func readMovesFromStdin() {

	scanner := bufio.NewScanner(os.Stdin)
	var text string
	var mutex = &sync.Mutex{}

	for {
		scanner.Scan()
		text = scanner.Text()

		mutex.Lock()
		movesToConvert = append(movesToConvert, text)
		mutex.Unlock()
	}
}

/*
* Controls queue to convert flow
*/
func convertor(in chan string, out chan string) {
	var mutex = &sync.Mutex{}

	for {
		if len(movesToConvert) > 0 {

			in <- movesToConvert[0]
			<-out

			mutex.Lock()
			movesToConvert = movesToConvert[1:]
			mutex.Unlock()
		}
	}
}

/*
* Wrapper around ffmpeg program starter
* read bytes from stdin combined with stderr
*/
func convertAvi(data chan string, out chan string) {
	for {
		select {
		case toConvert := <-data:

			if _, err := os.Stat(toConvert); os.IsNotExist(err) {
				fmt.Println(fmt.Sprintf("File %s does not exists", toConvert))
				out <- toConvert

			} else {

				fmt.Println("------>START TO CONVERT", toConvert, "<----")

				moveName := strings.TrimSuffix(toConvert, "avi")

				cmdName := fmt.Sprintf("ffmpeg -i %s -c:v libx264 -b:v 1000k -c:a aac -f mp4  %smp4 -y 2>&1", toConvert, moveName)
				cmd := exec.Command("sh", "-c", cmdName)
				stdout, _ := cmd.StdoutPipe()
				err := cmd.Start()

				if err != nil {
					fmt.Println(fmt.Sprintf("Error in cmt.Start: %s", err.Error()))
					return
				}

				oneByte := make([]byte, 8)
				for {
					_, err := stdout.Read(oneByte)
					if err != nil {

						//Reset duration
						duration = 0
						out <- toConvert
						break
					}
					allRes += string(oneByte)
                    parseString(allRes)
				}
			}
		}
	}
}

func main() {

	in := make(chan string, 1)
	out := make(chan string)

	wg := sync.WaitGroup{}
	wg.Add(3)

	go readMovesFromStdin()
	go convertor(in, out)
	go convertAvi(in, out)

	wg.Wait()
}
